﻿using UnityEngine;
using System.Collections;

public class PlayerHitbox : MonoBehaviour {

    public PlayerController Controller;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject && other.gameObject.GetComponent<EnemyShot>())
        {
            Hit();
            other.gameObject.GetComponent<EnemyShot>().Kill();
        }
        if (other.gameObject && other.gameObject.GetComponent<BombPowerUp>())
        {
            Controller.BombCount = 1;
            other.gameObject.GetComponent<BombPowerUp>().Kill();
        }
    }

    public void Hit()
    {
        Controller.Hit();
    }
}
