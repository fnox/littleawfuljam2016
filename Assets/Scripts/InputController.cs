﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {

    private float EscapeTimer = 0f;

    void Start()
    {
        StartCoroutine(InputRoutine());
    }
	
	public IEnumerator InputRoutine () {
        while (true)
        {
            if (GameController.GameActive)
            {
                if (GameController.PlayerInstance != null)
                {
                    var ply = GameController.PlayerInstance;
                    ply.Move(Mathf.Clamp((Input.GetAxis("Horizontal") + Input.GetAxis("Vertical")) * -1f, -1f, 1f));
                    if (Input.GetButton("Fire1")) ply.Shoot();
                    if (Input.GetButton("Fire2")) ply.Bomb();
                }
            }
            if (Input.GetButtonDown("Submit"))
                GameController.TogglePause();
            if (Input.GetButton("Escape")) EscapeTimer += Time.deltaTime;
            if (Input.GetButtonUp("Escape")) EscapeTimer = 0f;
            if (EscapeTimer >= 3f) GameController.Quit();
            yield return Time.unscaledDeltaTime;
        }
	}
}
