﻿using UnityEngine;
using System.Collections;

public class EnemyShot : MonoBehaviour, AudioProcessor.AudioCallbacks
{

    public SpriteRenderer sprite;
    public ScaleWithZ scaleScript;
    public float Scale = 1f;
    public float DefaultScale = 0f;
    public float Speed = 1f;
    private AudioProcessor processor;

    void Start()
    {
        DefaultScale = sprite.transform.localPosition.x;
        processor = GameController.MusicInstance.GetComponent<AudioProcessor>();
        processor.addAudioCallback(this);
    }

    public void OnBeatDetected()
    {
        Scale = 1.3f;
    }

    public void Kill()
    {
        Destroy(gameObject);
    }

    public void OnDestroy()
    {
        processor.removeAudioCallback(this);
    }

    public void OnSpectrum(float[] spectrum)
    {

    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Scale > 1f) Scale = Mathf.Clamp(Scale - Time.deltaTime, 1f, 2f);
        scaleScript.Modifier = Scale;
        transform.position -= Vector3.right * Time.deltaTime * Speed;
        if (transform.position.x <= -GameController.SpawnerInstance.OffScreenX)
        {
            Destroy(gameObject);
        }
    }
}
