﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour
{

    public AudioSource Source;

    public bool MusicStarted = false;

    public bool MusicStopped = false;

    public bool MusicEnded = false;

    public bool MusicError = false;

    public float MusicDuration = 0f;

    public float TimePassed = 0f;


	// Use this for initialization
	IEnumerator Start () {
        GameController.MusicInstance = this;
        var www = new WWW("file://" + GameController.MusicPath);
        var clip = www.audioClip; 
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
        {
            MusicError = true;
            Debug.LogError(www.error);
            yield break;
        }
        Source.clip = www.audioClip;
        Source.Play();
        MusicStarted = true;
        MusicDuration = Source.clip.length;
        StartCoroutine(PlayingRoutine());
    }

    public void StopMusic()
    {
        Source.Pause();
        MusicStopped = true;
    }

    public void StartMusic()
    {
        Source.UnPause();
        MusicStopped = true;
    }

    private IEnumerator PlayingRoutine()
    {
        while (!MusicEnded)
        {
            if (!MusicStopped) TimePassed += Time.deltaTime;
            if (TimePassed >= MusicDuration) MusicEnded = true;
            yield return null;
        }
    }
	
}
