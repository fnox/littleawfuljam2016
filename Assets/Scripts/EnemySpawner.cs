﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour, AudioProcessor.AudioCallbacks
{

    public List<GameObject> EnemyPrefabs;

    private AudioProcessor processor;

    public float OffScreenX = 0f;

    private float CurAngle = 0f;

    private bool Polarity = true;

    private bool SkipBeat = false;

    private bool SpawnEnemies = false;

    private bool StartedWinTransition = false;

    // Use this for initialization
    IEnumerator Start () {
        yield return null;
        GameController.SpawnerInstance = this;
        processor = GameController.MusicInstance.GetComponent<AudioProcessor>();
        processor.addAudioCallback(this);
        OffScreenX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 1.1f, Screen.height / 2f)).x;
        GameController.GameActive = true;
        yield return new WaitForSeconds(3f);
        SpawnEnemies = true;
    }

    public void SpawnEnemy()
    {
        if (!SpawnEnemies || GameController.MusicInstance.MusicEnded) return;
        SkipBeat = !SkipBeat;
        if (!SkipBeat) return;
        //Debug.Log("Spawning enemy");
        var size = 1f / EnemyPrefabs.Count;
        for (int i=1; i<=EnemyPrefabs.Count; i++)
        {
            if (GameController.MusicHue >= (i - 1) * size && GameController.MusicHue <= i * size)
            {
                var curprefab = EnemyPrefabs[i - 1];
                var instance = (GameObject)Instantiate(curprefab, new Vector3(OffScreenX * 1.5f, 0f), Quaternion.Euler(CurAngle, 0f, 0f));
                instance.SendMessage("Init", CurAngle);
                instance.transform.SetParent(transform);
                GameController.EnemiesSpawned++;
                if (Polarity) instance.SendMessage("ToggleRotationDirection");
                return;
            }
        }
    }

    public void OnBeatDetected()
    {
        SpawnEnemy();
    }
    public void OnSpectrum(float[] spectrum)
    {

    }

    // Update is called once per frame
    void Update () {
        CurAngle += GameController.MusicHue;
        Polarity = GameController.MusicHue >= 0.5f;
        if (GameController.MusicInstance.MusicEnded && GameController.Enemies.Count == 0 && ! StartedWinTransition)
        //if (true && !StartedWinTransition)
        {
            SpawnEnemies = false;
            GameController.WinTransition();
            StartedWinTransition = true;
        }
    }
}
