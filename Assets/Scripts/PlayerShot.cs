﻿using UnityEngine;
using Vectrosity;
using System.Collections;
using System.Collections.Generic;

public class PlayerShot : MonoBehaviour {

    public float Width = 0.05f;

    public float TimeAlive = 0.35f;

    public VectorLine Line;

    public List<Vector3> Points;

    public Color LineColor = new Color(1f, 1f, 1f, 1f);

    private float CurTime = 0f;

	// Use this for initialization
	void Start () {
        Points.Add(transform.position);
        Points.Add(transform.position + Vector3.right * 100);
        Line = new VectorLine("PlayerShot", Points, Width * Screen.height, LineType.Continuous);
        CurTime = TimeAlive;
	}

	// Update is called once per frame
	void FixedUpdate () {
        Line.Draw3D();
        CurTime -= Time.deltaTime;
        if (CurTime < 0f)
        {
            VectorLine.Destroy(ref Line, gameObject);
        }
        else
        {
            var delta = CurTime / TimeAlive;
            Line.SetColor(new Color(1f, 1f, 1f, delta));
            Line.SetWidth(Line.lineWidth * (delta / 2f + 0.5f));
        }
	}
}
