﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Prime31.TransitionKit;

public class MovieLoader : MonoBehaviour {

    public MovieTexture movie;
    public RawImage image;
    public int TransitionScene;

	// Use this for initialization
	IEnumerator Start () {
        image.texture = movie;
        movie.loop = false;
        movie.Play();
        image.SetNativeSize();
        var scale = Screen.width / image.texture.width;
        image.transform.localScale = new Vector3(scale, scale, scale);
        yield return new WaitForSeconds(movie.duration);
        var fader = new FadeTransition()
        {
            nextScene = TransitionScene,
            fadedDelay = 0.2f,
            fadeToColor = Color.black
        };
        TransitionKit.instance.transitionWithDelegate(fader);
    }

}
