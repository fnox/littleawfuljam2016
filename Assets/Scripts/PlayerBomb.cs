﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class PlayerBomb : MonoBehaviour, AudioProcessor.AudioCallbacks
{

    public Vector3 origin;
    public float Timer = 3f;
    public List<Vector3> points;
    public float Speed = 100f;
    public float Width = 1f;
    public float Radius = 1f;
    public VectorLine Circle;
    private AudioProcessor processor;
    public RaycastHit[] hits = new RaycastHit[1024];

	// Use this for initialization
	void Start () {
        origin = transform.position;
        points = new List<Vector3>();
        for (int i = 0; i <= 360; i++)
        {
            var rad = ((float) i) / 120f * 360f * Mathf.Deg2Rad;
            points.Add(new Vector3(Mathf.Cos(rad), Mathf.Sin(rad), 0f) * Radius + origin);
        }
        Circle = new VectorLine("BombCircle", points, 1f, LineType.Continuous);
        Circle.MakeCircle(Vector3.zero, Radius, 360, 0f);
        Circle.drawTransform = transform;
        processor = GameController.MusicInstance.GetComponent<AudioProcessor>();
        processor.addAudioCallback(this);
    }
	
    public void OnBeatDetected()
    {
        Width = 1.3f;
    }

    public void OnSpectrum(float[] spectrum)
    {

    }

    void OnDestroy()
    {
        processor.removeAudioCallback(this);
    }

	// Update is called once per frame
	void FixedUpdate () {
        Timer -= Time.deltaTime;
        Radius += Speed * Time.deltaTime;
        if (Width > 1f) Width = Mathf.Clamp(Width - Time.deltaTime, 1f, 2f);
        if (Timer < 0f)
        {
            VectorLine.Destroy(ref Circle, gameObject);
        }
        else
        {
            Physics.SphereCastNonAlloc(origin, Radius, Vector3.up, hits);
            foreach(var hit in hits)
            {
                if (hit.collider != null && hit.collider.gameObject != null)
                {
                    if (hit.collider.gameObject.GetComponent<EnemyHitbox>() != null)
                    {
                        hit.collider.gameObject.GetComponent<EnemyHitbox>().Hit();
                    }
                    if (hit.collider.gameObject.GetComponent<EnemyShot>() != null) hit.collider.gameObject.GetComponent<EnemyShot>().Kill();
                }
            }
            Circle.MakeCircle(Vector3.zero, Radius, 360, 0f);
            Circle.SetWidth(Width * Screen.width * 0.01f);
            Circle.Draw3D();
        }
    }
}
