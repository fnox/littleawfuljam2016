﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MusicListItem : MonoBehaviour {
    public Text MusicText;
    public string ActualPath;
    public MainMenuView View;
    
    public void OnClick()
    {
        View.SelectTrack(ActualPath);
    }
}
