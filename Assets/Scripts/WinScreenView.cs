﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WinScreenView : MonoBehaviour {

    public Image background;

    public Text EnemiesText;

    public Text MaxStreakText;

    public CanvasGroup Container;

    IEnumerator Start()
    {
        Container.alpha = 0f;
        var color = new HSBColor(GameController.MusicHue, 1f, 1f).ToColor();
        color = new Color(1f - color.r, 1f - color.g, 1f - color.b);
        background.color = color;
        EnemiesText.text = GameController.EnemiesKilled + "/" + GameController.EnemiesSpawned + " KILLED";
        MaxStreakText.text = (GameController.MaxStreak == GameController.EnemiesSpawned ? "PERFECT!!!" : "MAX " + GameController.MaxStreak + " STREAK");
        while (Container.alpha <= 1f)
        {
            Container.alpha += Time.unscaledDeltaTime * 0.5f;
            yield return null;
        }
        Container.alpha = 1f;
    }

    public void PlayAgain()
    {
        GameController.TransitionToGame();
    }

    public void BackToMenu()
    {
        GameController.OpenMainMenu();
    }

    public void Quit()
    {
        GameController.Quit();
    }

}
