﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseMenu : MonoBehaviour {

    public Text EnemyInfo;
    public Text StreakInfo;

	// Use this for initialization
	void Start () {
        EnemyInfo.text = GameController.EnemiesKilled + "/" + GameController.EnemiesSpawned + " DEAD";
        StreakInfo.text = GameController.CurrentStreak + " STREAK";
	}

    public void Continue()
    {
        GameController.UnpauseGame();
    }

    public void Quit()
    {
        GameController.UnpauseGame();
        GameController.OpenMainMenu();
    }
	
}
