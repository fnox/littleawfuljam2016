﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ScaleWithZ : MonoBehaviour {
    [Range(0,1)]
    public float Offset = 1f;

    public float Modifier = 1f;

	// Update is called once per frame
	void Update () {
        var amount = 1f + transform.position.z / GameController.WorldDepth;
        transform.localScale = new Vector3(amount * Offset * Modifier, amount * Offset * Modifier, amount * Offset * Modifier);
	}
}
