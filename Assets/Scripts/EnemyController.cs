﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    public bool Alive = true;

    public GameObject ShotPrefab;

    public GameObject BombPrefab;

    public GameObject ExplosionPrefab;

    public Transform ShootPosition;

    public int Life = 0;

    public float HorizontalSpeed = 5f;

    public float AngularSpeed = 1f;

    public float CurrentAngle = 0f;

    public float ShotSize = 1f;

    public float ShotRate = 0.5f;

    public SpriteRenderer EnemySprite;

	// Use this for initialization
	void Start () {

    }

    protected void Init(float angle)
    {
        CurrentAngle = angle;
        StartCoroutine(MoveRoutine());
        StartCoroutine(ShootRoutine());
        GameController.Enemies.Add(this);
    }
	
    public void Kill()
    {
        Debug.Log("Enemy dead");
        Alive = false;
        GameController.EnemiesKilled++;
        GameController.CurrentStreak++;
        StartCoroutine(Die());
    }

    public IEnumerator Die()
    {
        EnemySprite.enabled = false;
        Instantiate(ExplosionPrefab, EnemySprite.transform.position, EnemySprite.transform.rotation);
        if (Random.Range(0, 50) == 25) {
            Instantiate(BombPrefab, ShootPosition.position, ShootPosition.localRotation);
        } 
        yield return new WaitForSeconds(0.5f);
        DestroyImmediate(gameObject);
    }

    public void Hit()
    {
        Life--;
        if (Life == 0) Kill();
    }

    public void ToggleRotationDirection()
    {
        AngularSpeed *= -1f;
    }

    void OnDestroy()
    {
        GameController.Enemies.Remove(this);
        Alive = false;
    }

    public IEnumerator MoveRoutine()
    {
        while (Alive)
        {
            transform.position = new Vector3(transform.position.x - HorizontalSpeed * Time.deltaTime, transform.position.y, transform.position.z);
            CurrentAngle += AngularSpeed * Time.deltaTime;
            transform.localRotation = Quaternion.Euler(CurrentAngle, 0f, 0f);
            yield return null;
        }
    }

    public IEnumerator ShootRoutine()
    {
        while (Alive)
        {
            yield return new WaitForSeconds(2f);
            var shot = (GameObject) Instantiate(ShotPrefab, ShootPosition.position, ShootPosition.localRotation);
            shot.transform.SetParent(transform.parent);
        }
    }

    void FixedUpdate()
    {
        if (transform.position.x <= -GameController.SpawnerInstance.OffScreenX)
        {
            Destroy(gameObject);
            Alive = false;
        }
    }

}
