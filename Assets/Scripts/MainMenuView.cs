﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;

public class MainMenuView : MonoBehaviour {


    public CanvasGroup ContainerOne;

    public CanvasGroup ContainerTwo;

    public RectTransform List;

    public GameObject ListItem;

	// Use this for initialization
	void Start () {
        FillMusicList();
    }

    public void QuitGame()
    {
        GameController.Quit();
    }

    public void PlayGame()
    {
        ContainerOne.alpha = 0f;
        ContainerOne.blocksRaycasts = false;
        ContainerTwo.alpha = 1f;
    }
	
    public void SelectTrack( string name)
    {
        GameController.MusicPath = name;
        GameController.TransitionToGame();
    }

    public void FillMusicList()
    {
        var files = System.IO.Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, "Music"), "*.ogg");
        foreach(string file in files)
        {
            var obj = (GameObject) Instantiate(ListItem);
            obj.transform.SetParent(List);
            var script = obj.GetComponent<MusicListItem>();
            script.MusicText.text = Path.GetFileNameWithoutExtension(file);
            script.MusicText.text = script.MusicText.text.Substring(0, Mathf.Min(script.MusicText.text.Length, 60));
            script.ActualPath = file;
            script.View = this;
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
