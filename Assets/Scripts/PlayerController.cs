﻿using UnityEngine;
using Vectrosity;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

    private float Angle;

    private int Health = 1;

    public bool Invincible = false;

    private bool CanMove = true;

    private RaycastHit[] hits = new RaycastHit[1024];

    public int BombCount = 1;

    public float Speed;

    public float ShotSize;

    public float ShotRate;

    private float LastBomb = 0f;

    private float LastShot = 0f;

    private float PlayerX = 0f;

    public GameObject PauseMenuPrefab;

    public GameObject PlayerShotPrefab;

    public GameObject PlayerBombPrefab;

    public GameObject ExplosionPrefab;

    public ColorWithMusic PlayerColor;

    public SpriteRenderer PlayerSprite;

    public Transform ShootPosition;

	// Use this for initialization
	void Start () {
        GameController.PlayerInstance = this;
        GameController.SetUpGame();
        PlayerX = transform.position.x;
    }

    void OnApplicationFocus(bool focusStatus)
    {
        Debug.Log((!focusStatus ? "Application lost focus" : "Application regained focus"));
        if (!focusStatus) GameController.PauseGame();
    }

    public void Hit()
    {
        Debug.Log("Player Hit");
        if (Invincible) return;
        Health--;
        if (Health <= 0) StartCoroutine(Die());
    }

    public IEnumerator Die()
    {
        GameController.MaxStreak = Mathf.Max(GameController.MaxStreak, GameController.CurrentStreak);
        GameController.CurrentStreak = 0;
        Instantiate(ExplosionPrefab, PlayerSprite.transform.position, PlayerSprite.transform.rotation);
        CanMove = false;
        Invincible = true;
        transform.position = new Vector3(-GameController.SpawnerInstance.OffScreenX, 0f, 0f);
        while (transform.position.x < PlayerX)
        {
            transform.position = transform.position + Vector3.right * 3f * Time.deltaTime;
            yield return null;
        }
        transform.position = new Vector3(PlayerX, transform.position.y, transform.position.z);
        Health = 1;
        CanMove = true;
        yield return new WaitForSeconds(1f);
        Invincible = false;
        yield return null;
    }

    public void Move(float increment)
    {
        if (!CanMove) return;
        Angle += increment * Speed;
    }

    public void Bomb()
    {
        if (!CanMove) return;
        if (BombCount > 0 && LastBomb + ShotRate * 4f < Time.time)
        {
            var curpos = transform.position;
            BombCount--;
            LastBomb = Time.time;
            var bomb = Instantiate(PlayerBombPrefab, ShootPosition.transform.position, Quaternion.Euler(Vector3.zero));
        }
    }

    public void Shoot()
    {
        if (!CanMove) return;
        if (LastShot + ShotRate < Time.time)
        {
            Physics.CapsuleCastNonAlloc(ShootPosition.transform.position, ShootPosition.transform.position + Vector3.right * 100f, ShotSize * PlayerSprite.transform.localScale.x, Vector3.right, hits);
            foreach(var hit in hits)
            {
                if (hit.collider != null && hit.collider.gameObject != null)
                    if (hit.collider.gameObject.GetComponent<EnemyHitbox>() != null)
                    {
                        hit.collider.gameObject.GetComponent<EnemyHitbox>().Hit();
                    }
            }
            var obj = (GameObject) Instantiate(PlayerShotPrefab, ShootPosition.transform.position, Quaternion.Euler(Vector3.zero));
            obj.GetComponent<PlayerShot>().Width *= PlayerSprite.transform.localScale.x;
            LastShot = Time.time;
        }
    }

    public GameObject PauseMenu()
    {
        return (GameObject) Instantiate(PauseMenuPrefab, Vector3.zero, Quaternion.Euler(Vector3.zero));
    }

    public void CloseMenu(GameObject menu)
    {
        DestroyImmediate(menu);
    }

    public void Win()
    {
        StartCoroutine(WinAnimation());
    }

    public IEnumerator WinAnimation()
    {
        Invincible = true;
        CanMove = false;
        yield return new WaitForSeconds(1f);
        var speed = 5f;
        var points = new List<Vector3>();
        points.Add(new Vector3(-GameController.SpawnerInstance.OffScreenX, PlayerSprite.transform.position.y, PlayerSprite.transform.position.z));
        points.Add(PlayerSprite.transform.position);
        var line = new VectorLine("Winline", points, Screen.height * 0.05f, LineType.Continuous);
        var color = new HSBColor(GameController.MusicHue, 1f, 1f).ToColor();
        color = new Color(1f - color.r, 1f - color.g, 1f - color.b);
        line.lineWidth = Screen.height * 0.01f;
        line.Draw3DAuto();
        line.SetColor(color);
        while (transform.position.x <= GameController.SpawnerInstance.OffScreenX * 2f)
        {
            speed += Time.deltaTime;
            transform.position = new Vector3(transform.position.x + Time.deltaTime * speed, transform.position.y, transform.position.z);
            points.Add(PlayerSprite.transform.position);
            line.SetColor(color);
            yield return null;
        }
        while (line.lineWidth <= Screen.height * 2f)
        {
            line.lineWidth = line.lineWidth + (line.lineWidth * 10f) * Time.deltaTime;
            yield return null;
        }
        GameController.OpenWinScreen();
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawLine(ShootPosition.transform.position, ShootPosition.transform.position + Vector3.right * 100f);
        Gizmos.DrawWireSphere(ShootPosition.transform.position + Vector3.right * 100f, ShotSize);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.localRotation = Quaternion.Euler(new Vector3(Angle, 0f, 0f));
        PlayerColor.Alpha = (Invincible && PlayerColor.Alpha >= 0.5f ? PlayerColor.Alpha -= Time.deltaTime * 10f : 1f);
    }
}
