﻿using UnityEngine;
using System.Collections;

public class ColorWithMusic : MonoBehaviour {

    private HSBColor SpriteColor;

    public float Brightness = 1f;

    public float Alpha = 1f;

	// Use this for initialization
	void Start () {
        SpriteColor = new HSBColor(GameController.MusicHue, 1f, 1f, 1f);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        SpriteColor = new HSBColor(Mathf.Lerp(SpriteColor.h, GameController.MusicHue, Time.deltaTime * 2f), 1f, Brightness, Alpha);
        GetComponent<SpriteRenderer>().color = SpriteColor.ToColor();
    }
}
