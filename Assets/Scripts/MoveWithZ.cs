﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MoveWithZ : MonoBehaviour {

    public Vector3 Offset;

    public Transform SpriteScale;

    // Update is called once per frame
    void Update()
    {
        var amount = SpriteScale.localScale.x;
        transform.localPosition = new Vector3(Offset.x + amount, Offset.y, transform.localPosition.z);
    }
}
