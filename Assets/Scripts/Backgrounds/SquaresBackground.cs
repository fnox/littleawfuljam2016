﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class SquaresBackground : BackgroundEffect {

    private bool Kill = false;

    private float Depth = 50f;

    private float Separation = 3f;

    private float DotSize = 0.275f;

    private int DotsCount = 0;

    private Vector2 MinVector = new Vector2(-15f, -10f);

    private Vector2 MaxVector = new Vector2(30f, 10f);

    private float MaxX;

    private HSBColor DotColor;

    private List<Vector3> Points = new List<Vector3>();

    private VectorLine Dots;

    public override IEnumerator TransitionIn()
    {
        var MaxX = MinVector.x;
        for (float i=MinVector.x; i<=MaxVector.x; i += Separation)
        {
            for (float j=MinVector.y; j<=MaxVector.y; j += Separation)
            {
                Points.Add(new Vector3(i, j, Depth));
            }
            MaxX = Mathf.Max(MaxX, i);
        }

        DotsCount = Points.Count;

        Dots = new VectorLine("Squares", Points, DotSize * Screen.height, LineType.Points);
        Dots.drawTransform = GameController.BackgroundInstance.transform;
        Dots.Draw3DAuto();
        var timer = 1f;
        DotColor = new HSBColor(GameController.MusicHue, 1f, 1f, Mathf.Clamp01(1f - timer));
        while ((timer -= Time.deltaTime * 2f) >= 0f)
        {
            ScrollLines();
            DotColor = new HSBColor(DotColor.h, 1f, 1f, Mathf.Clamp01(1f - timer));
            Dots.SetColor(DotColor.ToColor());
            yield return null;
        }

        yield return null;
    }

    public override void OnBeatDetected()
    {

    }

    public override void OnSpectrum(float[] spectrum)
    {

    }

    public override IEnumerator IdleRoutine()
    {
        while (!Kill)
        {
            ScrollLines();
            DotColor = HSBColor.Lerp(DotColor, new HSBColor(GameController.MusicHue, 1f, 1f, 1f), Time.deltaTime * 2f);
            Dots.SetColor(DotColor.ToColor());
            yield return null;
        }
    }

    private void ScrollLines()
    {
        var maxx = MinVector.x;
        foreach(Vector3 point in Dots.points3)
        {
            maxx = Mathf.Max(maxx, point.x);
        }
        for (int i=0; i< Dots.points3.Count; i++)
        {
            Dots.points3[i] -= Vector3.right * GameController.ScrollSpeed * Time.deltaTime;
            if (Dots.points3[i].x <= MinVector.x)
            {
                Dots.points3.Add(new Vector3(MaxVector.x + Separation - GameController.ScrollSpeed * Time.deltaTime, Dots.points3[i].y, Depth));
                Dots.points3.RemoveAt(i);
            }
        }
    }

    public override IEnumerator TransitionOut()
    {
        Kill = true;
        var timer = 1f;
        while ((timer -= Time.deltaTime) >= 0f)
        {
            ScrollLines();
            DotColor = new HSBColor(DotColor.h, 1f, 1f, Mathf.Clamp01(timer));
            Dots.SetColor(DotColor.ToColor());
            yield return null;
        }
        yield return null;
        Dots.StopDrawing3DAuto();
        VectorLine.Destroy(ref Dots, gameObject);
    }
}
