﻿using UnityEngine;
using System.Collections;

public class BackgroundEffect : MonoBehaviour {

    public string Name = "Parent";

	public virtual IEnumerator TransitionIn()
    {
        yield return null;
    }

    public virtual void OnBeatDetected()
    {

    }
    
    public virtual void OnSpectrum(float[] spectrum)
    {

    }

    public virtual IEnumerator IdleRoutine()
    {
        yield return null;
    }

    public virtual IEnumerator TransitionOut()
    {
        yield return null;
    }

}
