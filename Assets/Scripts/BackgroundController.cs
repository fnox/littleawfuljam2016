﻿using UnityEngine;
using UnityEngine.UI;
using Vectrosity;
using System.Collections.Generic;
using System.Collections;

public class BackgroundController : MonoBehaviour, AudioProcessor.AudioCallbacks
{

    public VectorLine Dots;

    public float BeatDecay = 0.5f;

    public float MaxWidth = 10f;

    public int TempoAttenuation = 90;

    public float Amplification = 100f;

    public float BeatSize = 1f;

    //public Text BPMText;

    //public Image HueImage;

    //public List<Vector2> Points;

    //public List<Color32> Colors;

    public List<GameObject> BackgroundPrefabs;

    private BackgroundEffect CurrentBackground;

    private List<string> PrevBackgrounds = new List<string>();

    //private HSBColor CurColor = new HSBColor();

    private AudioProcessor processor;

    private static Random rnd = new Random();

    // Use this for initialization
    IEnumerator Start () {
        yield return null;
        GameController.BackgroundInstance = this;
        processor = GameController.MusicInstance.GetComponent<AudioProcessor>();
        processor.addAudioCallback(this);

        /*
        Dots = new VectorLine("BeatDot",  Points, MaxWidth, LineType.Points);

        Dots.SetColors(Colors);
        */
        StartCoroutine(SelectBackgroundRoutine());
    }

    public void SelectNextBackground()
    {
        if (PrevBackgrounds.Count > 0 && PrevBackgrounds.Count > BackgroundPrefabs.Count / 2) BackgroundPrefabs.RemoveAt(0);
        while (true)
        {
            var bg = BackgroundPrefabs[Mathf.RoundToInt(Random.Range(0, BackgroundPrefabs.Count - 1))];
            if (PrevBackgrounds.Contains(bg.name)) continue;
            else
            {
                var obj = (GameObject) Instantiate(bg, Vector3.zero, Quaternion.Euler(Vector3.zero));
                obj.transform.SetParent(transform);
                CurrentBackground = obj.GetComponent<BackgroundEffect>();
                break;
            }
        }
    }

    public IEnumerator SelectBackgroundRoutine()
    {
        SelectNextBackground();
        while (true)
        {
            yield return StartCoroutine(CurrentBackground.TransitionIn());
            yield return null;
            StartCoroutine(CurrentBackground.IdleRoutine());
            yield return new WaitForSeconds(GameController.BackgroundTransitionTime);
            yield return StartCoroutine(CurrentBackground.TransitionOut());
            SelectNextBackground();
            yield return null;
        }
    }

    public IEnumerator BackgroundDrawRoutine(){
        yield return null;
    }

    public void OnBeatDetected()
    {
        processor.tapTempo();
        if (CurrentBackground != null) CurrentBackground.OnBeatDetected();
        BeatSize = 1f;
    }
    public void OnSpectrum(float[] spectrum)
    {
        var total = 0f;
        var max = 0f;
        var maxIndex = 0;
        for (int i = 0; i < spectrum.Length; i++)
        {
            //Dots.points2[i] = new Vector2(Dots.points2[i].x, 200 + spectrum[i] * MaxWidth * Amplification);
            if (spectrum[i] > max)
            {
                maxIndex = i;
                max = spectrum[i];
            }
            total += spectrum[i];
        }
        GameController.MusicHue = max * 10f + maxIndex / spectrum.Length;
        if (CurrentBackground != null) CurrentBackground.OnSpectrum(spectrum);
        //Debug.Log("Spectrum: " + str);
    }
    // Update is called once per frame
    void Update () {
        if (processor == null) return;
        BeatSize = Mathf.Clamp01(BeatSize -= Time.deltaTime / BeatDecay);
        /*
        Dots.lineWidth = BeatSize * MaxWidth;
        Dots.Draw();
        BPMText.text = "" + GameController.MusicHue;
        CurColor = HSBColor.Lerp(CurColor, new HSBColor(GameController.MusicHue, 1f, 1f), BeatDecay * Time.deltaTime);
        HueImage.color = CurColor.ToColor();*/

    }
}
