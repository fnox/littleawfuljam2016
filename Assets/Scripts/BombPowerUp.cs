﻿using UnityEngine;
using System.Collections;

public class BombPowerUp : MonoBehaviour {

    public float Speed;

    public void Kill()
    {
        Destroy(gameObject);
    }

    void FixedUpdate()
    {
        transform.position -= Vector3.right * Time.deltaTime * Speed;
        if (transform.position.x <= -GameController.SpawnerInstance.OffScreenX)
        {
            Destroy(gameObject);
        }
    }
}
