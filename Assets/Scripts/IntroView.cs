﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IntroView : MonoBehaviour {

    public CanvasGroup container;

	// Use this for initialization
	IEnumerator Start () {
        while (container.alpha < 1f)
        {
            container.alpha += Time.unscaledDeltaTime;
            yield return null;
        }
        yield return new WaitForSeconds(2f);
        GameController.OpenMainMenu();
	}
	
}
