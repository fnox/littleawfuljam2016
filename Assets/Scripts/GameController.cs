﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Prime31.TransitionKit;

public static class GameController {

    public static PlayerController PlayerInstance;

    public static BackgroundController BackgroundInstance;

    public static MusicController MusicInstance;

    public static EnemySpawner SpawnerInstance;

    public static GameObject PauseMenuInstance;

    public static List<EnemyController> Enemies = new List<EnemyController>();

    public static bool GameActive = false;

    public static bool GamePaused = false;

    public static float ScrollSpeed = 1;

    public static float WorldDepth = 6f;

    public static float MusicHue = 0f;

    public static int EnemiesSpawned = 0;

    public static int EnemiesKilled = 0;

    public static int CurrentStreak = 0;

    public static int MaxStreak = 0;

    public static string MusicPath = Path.Combine(Path.Combine(Application.streamingAssetsPath, "Music"), "Pyro Flow - Kevin MacLeod.ogg");

    public static float BackgroundTransitionTime = 10f;

    public static void Quit()
    {
        Debug.Log("Quitting...");
        Application.Quit();
    }

    public static void TransitionToGame()
    {
        EnemiesSpawned = 0;
        EnemiesKilled = 0;
        CurrentStreak = 0;
        MaxStreak = 0;
        var fader = new FadeTransition()
        {
            nextScene = 4,
            fadedDelay = 0.2f,
            fadeToColor = Color.red
        };
        TransitionKit.instance.transitionWithDelegate(fader);
    }

    public static void LoadPreferences()
    {

    }

    public static void SavePreferences()
    {

    }
    
    public static void OpenWinScreen()
    {
        SceneManager.LoadScene("WinScreen");
    }

    public static void WinTransition()
    {
        PlayerInstance.Win();
    }

    public static void OpenMainMenu()
    {
        EnemiesSpawned = 0;
        EnemiesKilled = 0;
        CurrentStreak = 0;
        MaxStreak = 0;
        var fader = new WindTransition()
        {
            nextScene = 2,
            duration = 1f,
            size = 0.3f
        };
        TransitionKit.instance.transitionWithDelegate(fader);
    }

    public static void PauseGame()
    {
        Time.timeScale = 0f;
        if (PauseMenuInstance == null) PauseMenuInstance = PlayerInstance.PauseMenu();
        MusicInstance.StopMusic();
        GamePaused = true;
    }

    public static void UnpauseGame()
    {
        Time.timeScale = 1f;
        PlayerInstance.CloseMenu(PauseMenuInstance);
        MusicInstance.StartMusic();
        GamePaused = false;
    }

    public static void TogglePause()
    {
        if (GamePaused) UnpauseGame(); else PauseGame();
    }

    public static void SetUpGame()
    {
        Application.runInBackground = true;
    }
}
